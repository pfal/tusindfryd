﻿using System;
using Tusindfryd.Application;

namespace Tusindfryd
{
    class Program
    {
        static void Main(string[] args)
        {
            ProductionController production = new ProductionController();

            production.AddGreenhouse(1);
            production.AddWorker("PFA");
            production.AddProductiontray("1A", 3.0);
            production.AddProductiontray("1B", 3.0);

            production.AddFlower("Kornblomst", 30, 150, 0.01);
            production.AddFlower("Marguritter", 35, 140, 0.0225);

            production.StartProduction(1, "1A", "Kornblomst", 1000, DateTime.Now);
            production.StartProduction(1, "1B", "Marguritter", 888, DateTime.Now);

            production.AddCount(1, "PFA", 875, DateTime.Now);
            production.AddCount(2, "PFA", 745, DateTime.Now);


            production.PrintProduction(1);
            production.PrintProduction(2);

            Console.ReadLine();

        }
    }
}
