﻿using System;
using System.Collections.Generic;
using System.Text;
using Tusindfryd.Domain;

namespace Tusindfryd.Application
{
    public static class ProductiontrayRepository
    {
        private static List<Productiontray> productiontrayList = new List<Productiontray>();

        public static void Create(Productiontray productiontray)
        {
            productiontrayList.Add(productiontray);
        }

        public static Productiontray Get(string name)
        {
            foreach (Productiontray productiontray in productiontrayList)
            {
                if (name.Equals(productiontray.Name))
                {
                    return productiontray;
                }
                // return productiontrayList.Find(productiontray => name.Equals(productiontray.Name));
            }
            throw new Exception("Fandt ikke produktionsbakken");
        }
    }
}
