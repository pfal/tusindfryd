﻿using System;
using System.Collections.Generic;
using System.Text;
using Tusindfryd.Domain;

namespace Tusindfryd.Application
{
    public static class GreenhouseRepository
    {
        private static List<Greenhouse> greenhouseList = new List<Greenhouse>();

        public static void Create(Greenhouse greenhouse)
        {
            greenhouseList.Add(greenhouse);
        }

        public static Greenhouse Get(int number)
        {
            foreach (Greenhouse greenhouse in greenhouseList)
            {
                if (number == greenhouse.Number)
                {
                    return greenhouse;
                }
                // return greenhouseList.Find(greenhouse => number == greenhouse.Number);
            }
            throw new Exception("Fandt ikke drivhuset");
        }
    }
}
