﻿using System;
using System.Collections.Generic;
using System.Text;
using Tusindfryd.Domain;

namespace Tusindfryd.Application
{
    public static class FlowerRepository
    {
        private static List<Flower> flowerList = new List<Flower>();

        public static void Create(Flower flower)
        {
            flowerList.Add(flower);
        }

        public static Flower Get(string name)
        {
            foreach (Flower flower in flowerList)
            {
                if (name.Equals(flower.Name))
                {
                    return flower;
                }
                // return flowerList.Find(flower => name.Equals(flower.Name));
            }
            throw new Exception("Fandt ikke blomsten");
        }
    }
}
