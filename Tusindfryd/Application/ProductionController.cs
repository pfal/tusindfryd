﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Tusindfryd.Domain;

namespace Tusindfryd.Application
{
    public class ProductionController
    {
        // =======================================================
        // Production
        // =======================================================

        public void StartProduction(int greenhouse, string productiontray, string flower, int startAmount, DateTime startDate)
        {
            Flower chosenFlower = GetFlower(flower);
            double expectedAmount = CalcHalfLife(startAmount, chosenFlower.HalfLife, chosenFlower.ProductionTimeInDays);

            ProductionRepository.Create(new Production(GetGreenhouse(greenhouse), GetProductiontray(productiontray), GetFlower(flower), startDate, startAmount, Math.Ceiling(expectedAmount)));
        }
        
        // Midlertidig test metode.
        public void PrintProduction(int productionId)
        {
            Production chosenProduction = ProductionRepository.Get(productionId);

            if(chosenProduction.Count != null)
            {
            Console.WriteLine("==========================================================================" + $" Produktion: {chosenProduction.Id} " + "============================================================================\n");
            Console.WriteLine($"                                                      Drivhus: {chosenProduction.Greenhouse.Number}, Produktionsbakke: {chosenProduction.Productiontray.Name}, Blomst: {chosenProduction.Flower.Name}" +
                $"\n                                           Start dato: {chosenProduction.StartDate}, Start antal: {chosenProduction.StartAmount}, Forventet slut antal: {chosenProduction.ExpectedAmount}" +
                $"\n" +
                $"\n                                                                             Optælling: " +
                $"\n                Optalt af: {chosenProduction.Count.CheckedBy.Initials}, Optalt dato: {chosenProduction.Count.CountDate}, Antal talt: {chosenProduction.Count.CountedAmount}, Beregnet slut antal: {chosenProduction.Count.CalculatedEndAmount}, Afvigelse i procent: {chosenProduction.Count.DeviationInPercentage}\n");
            Console.WriteLine("=====================================================================================================================================================================\n");
            } 
            else
            {
                Console.WriteLine("==========================================================================" + $" Produktion: {chosenProduction.Id} " + "============================================================================");
                Console.WriteLine($"                                Drivhus: {chosenProduction.Greenhouse.Number}, Produktionsbakke: {chosenProduction.Productiontray.Name}, Blomst: {chosenProduction.Flower.Name}" +
                    $"\n                     Start dato: {chosenProduction.StartDate}, Start antal: {chosenProduction.StartAmount}, Forventet slut antal: {chosenProduction.ExpectedAmount}");
                Console.WriteLine("=====================================================================================================================================================================\n");
            }


        }
        // =======================================================
        // Flower
        // =======================================================
        public void AddFlower(string name, int productionTimeInDays, int halfLife, double size)
        {
            FlowerRepository.Create(new Flower(name, productionTimeInDays, halfLife, size));
        }

        public Flower GetFlower(string name)
        {
            return FlowerRepository.Get(name);
        }

        // =======================================================
        // Greenhouse
        // =======================================================
        public void AddGreenhouse(int number)
        {
            GreenhouseRepository.Create(new Greenhouse(number));
        }

        public Greenhouse GetGreenhouse(int number)
        {
            return GreenhouseRepository.Get(number);
        }

        // =======================================================
        // Worker
        // =======================================================
        public void AddWorker(string initials)
        {
            WorkerRepository.Create(new Worker(initials));
        }

        public Worker GetWorker(string initials)
        {
            return WorkerRepository.Get(initials);
        }

        // =======================================================
        // Count
        // =======================================================
        public void AddCount(int productionId, string worker, int amountCounted, DateTime date)
        {
            Production chosenProduction = ProductionRepository.Get(productionId);

            double expectedAmount = chosenProduction.Flower.HalfLife;
            double deviationInPercentage = ((amountCounted - expectedAmount) / expectedAmount) / 100;
            // CalculatedEndAmount skal laves!
            chosenProduction.Count = new Count(GetWorker(worker), date, amountCounted, amountCounted, deviationInPercentage);
        }

        public Count GetCount(Count count)
        {
            return count;
        }

        // =======================================================
        // Productiontray
        // =======================================================
        public void AddProductiontray(string name, double size)
        {
            ProductiontrayRepository.Create(new Productiontray(name, size));
        }

        public Productiontray GetProductiontray(string name)
        {
            return ProductiontrayRepository.Get(name);
        }

        // =======================================================
        // Half life calculation
        // =======================================================
        public double CalcHalfLife(double startAmount, double halfLifeInDays, double productionTimeInDays)
        {
            double result = startAmount * Math.Pow(0.5, productionTimeInDays / halfLifeInDays);
            return result;
        }

    }
}
