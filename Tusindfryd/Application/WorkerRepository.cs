﻿using System;
using System.Collections.Generic;
using System.Text;
using Tusindfryd.Domain;

namespace Tusindfryd.Application
{
    public static class WorkerRepository
    {
        private static List<Worker> workerList = new List<Worker>();

        public static void Create(Worker worker)
        {
            workerList.Add(worker);
        }

        public static Worker Get(string initials)
        {
            foreach (Worker worker in workerList)
            {
                if (initials.Equals(worker.Initials))
                {
                    return worker;
                }
                // return wokerList.Find(worker => initials.Equals(worker.Initials));
            }
            throw new Exception("Fandt ikke medarbejderen");
        }
    }
}
