﻿using System;
using System.Collections.Generic;
using System.Text;
using Tusindfryd.Domain;

namespace Tusindfryd.Application
{
    public static class ProductionRepository
    {
        private static List<Production> productionList = new List<Production>();

        public static void Create(Production production)
        {
            productionList.Add(production);
        }

        public static Production Get(int id)
        {
            foreach (Production production in productionList)
            {
                if (id == production.Id)
                {
                    return production;
                }
                // return productionList.Find(production => id == production.Id);
            }
            throw new Exception("Fandt ikke produktionen");
        }
    }
}
