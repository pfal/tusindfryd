﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tusindfryd.Domain
{
    public class Productiontray
    {
        public string Name { get; set; }
        public double Size { get; set; }

        public Productiontray(string name, double size)
        {
            this.Name = name;
            this.Size = size;
        }
    }
}
