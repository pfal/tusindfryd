﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tusindfryd.Domain
{
    public class Worker
    {
        public string Initials { get; set; }

        public Worker(string initials)
        {
            this.Initials = initials;
        }
    }
}
