﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tusindfryd.Domain
{
    public class Greenhouse
    {
        public int Number { get; set; }

        public Greenhouse(int number)
        {
            this.Number = number;
        }
    }
}
