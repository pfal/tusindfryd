﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tusindfryd.Domain
{
    public class Production
    {
        public Greenhouse Greenhouse { get; set; }
        public Productiontray Productiontray { get; set; }
        public Flower Flower { get; set; }
        public Count Count { get; set; }
        public DateTime StartDate { get; set; }
        public int StartAmount { get; set; }
        public double ExpectedAmount { get; set; }
        public bool Finished { get; set; }
        public int Id { get; } = counter;

        private static int counter = 0;

        public Production(Greenhouse greenhouse, Productiontray productiontray, Flower flower, Count count, DateTime startDate, int startAmount, double expectedAmount, bool finished, int id)
        {
            this.Greenhouse = greenhouse;
            this.Productiontray = productiontray;
            this.Flower = flower;
            this.Count = count;
            this.StartDate = startDate;
            this.StartAmount = startAmount;
            this.ExpectedAmount = expectedAmount;
            this.Finished = finished;
        }

        public Production(Greenhouse greenhouse, Productiontray productiontray, Flower flower, DateTime startDate, int startAmount, double expectedAmount) : this (greenhouse, productiontray, flower, null, startDate, startAmount, expectedAmount, false, System.Threading.Interlocked.Increment(ref counter))
        {
        }
    }
}
