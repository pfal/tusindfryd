﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tusindfryd.Domain
{
    public class Count
    {
        public DateTime CountDate { get; set; }
        public int CountedAmount { get; set; }
        public int CalculatedEndAmount { get; set; }
        public double DeviationInPercentage { get; set; }
        public Worker CheckedBy { get; set; }

        public Count(Worker checkedBy, DateTime countDate, int countedAmount, int calculatedEndAmount, double deviationInPercentage)
        {
            this.CheckedBy = checkedBy;
            this.CountDate = countDate;
            this.CountedAmount = countedAmount;
            this.CalculatedEndAmount = calculatedEndAmount;
            this.DeviationInPercentage = deviationInPercentage;
        }
    }
}
