﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tusindfryd.Domain
{
    public class Flower
    {
        public string Name { get; set; }
        public int ProductionTimeInDays { get; set; }
        public int HalfLife { get; set; }
        public double Size { get; set; }

        public Flower(string name, int productionTimeInDays, int halfLife, double size)
        {
            this.Name = name;
            this.ProductionTimeInDays = productionTimeInDays;
            this.HalfLife = halfLife;
            this.Size = Size;
        }
    }
}
